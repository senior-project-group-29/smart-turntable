# Documents

Each cycle we create the following documents:

## Launch

The launch document to plan what we want to accomplish that cycle

## REQ

The requirements document

## DES

The design document

## TES

The test plan

## PRE

The end-of-cycle presentation slide deck